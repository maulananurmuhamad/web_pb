<?php namespace Mnm\Produkpb\Models;

use Model;

/**
 * Model
 */
class Produk extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'mnm_produkpb_';

    /*Relations*/
    public $attachOne= [
      'img_produk' => 'System\Models\File',
      'img_detailproduk' => 'System\Models\File'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
