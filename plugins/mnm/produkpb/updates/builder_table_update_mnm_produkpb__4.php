<?php namespace Mnm\Produkpb\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMnmProdukpb4 extends Migration
{
    public function up()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->string('keterangan', 65535)->nullable()->unsigned(false)->default(null)->change();
            $table->text('deskripsi')->nullable()->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->text('keterangan')->nullable()->unsigned(false)->default(null)->change();
            $table->string('deskripsi', 191)->nullable()->unsigned(false)->default(null)->change();
        });
    }
}
