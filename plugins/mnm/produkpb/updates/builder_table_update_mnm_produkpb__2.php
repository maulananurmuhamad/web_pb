<?php namespace Mnm\Produkpb\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMnmProdukpb2 extends Migration
{
    public function up()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->string('slug')->nullable();
            $table->text('keterangan')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->dropColumn('slug');
            $table->text('keterangan')->nullable(false)->change();
        });
    }
}
