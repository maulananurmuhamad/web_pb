<?php namespace Mnm\Produkpb\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMnmProdukpb extends Migration
{
    public function up()
    {
        Schema::create('mnm_produkpb_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('title');
            $table->text('keterangan');
            $table->string('gambar');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mnm_produkpb_');
    }
}
