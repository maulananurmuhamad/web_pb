<?php namespace Mnm\Produkpb\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMnmProdukpb3 extends Migration
{
    public function up()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->string('deskripsi')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->dropColumn('deskripsi');
        });
    }
}
