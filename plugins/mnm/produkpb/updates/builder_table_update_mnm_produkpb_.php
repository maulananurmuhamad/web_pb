<?php namespace Mnm\Produkpb\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMnmProdukpb extends Migration
{
    public function up()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->dropColumn('gambar');
        });
    }
    
    public function down()
    {
        Schema::table('mnm_produkpb_', function($table)
        {
            $table->string('gambar', 191);
        });
    }
}
